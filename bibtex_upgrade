#!/usr/bin/env ruby

# Requires ruby 3.0+
# ruby -v

SCRIPT_VERSION = "2.5"

# A bit of vocabulary.
#
# An array is a regular structure. For instance, here's what it
# looks like:
#
# array = [1, 2, 3, 4]
#
# A hash is a hashmap, containing key-value pairs. Here's what it
# looks like:
#
# hashmap = {"john" => "male", "brenda" => "female"}
#
# ######
#
# In addition to regular String, Ruby has a nice thing called "Symbols".
# They behave more or less like strings, but they are much more powerful.
#
# Just know that they can be used as keys/values in hashes, and that
# this is the way to go.
#
# Here is the above array, using symbols
#
# hashmap = {"john" => :male, "brenda" => :female}

require "csv"
require 'bibtex'
require 'paint'
require './lib/cli_helpers'

# Constants used to defined headers in CSVs, can be safely
# modified to accommodate any changes in the format.
CSV_HEADER_FIRST_NAME = "First Name"
CSV_HEADER_LAST_NAME = "Last Name"

CSV_HEADER_JOURNAL_NAME = "Journal name"
CSV_HEADER_ISSN = "ISSN"
CSV_HEADER_IF = "IF 2020"

NOTE_ISSN = "ISSN"
NOTE_IF = "IF 2020"

NAME_HEADERS = [CSV_HEADER_FIRST_NAME, CSV_HEADER_LAST_NAME]
JOURNAL_HEADERS = [CSV_HEADER_JOURNAL_NAME, CSV_HEADER_ISSN, CSV_HEADER_IF]

######## FUNCTIONS ########
def process_file(original_bibtex_file_path, names_file_path, journals_file_path)

  # Keep tracks of the number of journal entries converted during the search.
  # { JOURNAL NAME } -> JOURNAL NAME
  number_of_double_braces_converted = 0

  # A set of journals found in `isiwok = OUI` entries, but which don't seem to exist
  # in the provided list of journals.
  journals_not_found = Set.new

  # Parse the bibtex file into a array-of-hash-like structure.
  # At the end of the day, we have all the BibTeX entry into an array, with
  # all the fields accessible in a hash-like manner.
  print_and_flush "Loading the bibliography… "
  file_content = File.open(original_bibtex_file_path, "r:ISO-8859-1")
  file_content = file_content.read().encode("UTF-8", "ISO-8859-1")
  bibtex_content = BibTeX.parse(file_content)
  print_and_flush Paint["DONE\n", :green]

  # Parse the CSV files. They need to have headers, so we can query
  # each individual value, for each line, using the name of the column.
  name_list = CSV.read(names_file_path, :headers => true)
  journal_list = CSV.read(journals_file_path, :headers => true)

  # Retrieve the headers parsed in each CSV files.
  name_list_headers = name_list.headers
  journal_list_headers = journal_list.headers

  # Checks whether headers are correct. If not, a warning is printed.
  #
  # In particular, we check that the array created by removing all elements contained
  # in name_list_headers from NAME_HEADERS returns an empty array, thus ensuring
  # that all necessary headers are present.
  unless ((NAME_HEADERS - name_list_headers).empty?)
      print_and_flush Paint["WARNING: The CSV headers of \"#{File.basename(names_file_path)}\" seem to be incorrect (expected: #{NAME_HEADERS}).\n", :yellow]
  end

  unless ((JOURNAL_HEADERS - journal_list_headers).empty?)
      print_and_flush Paint["WARNING: The CSV headers of \"#{File.basename(journals_file_path)}\" seem to be incorrect (expected: #{JOURNAL_HEADERS}).\n", :yellow]
  end

  # Turn the array returned by the CSV parser into a hash, so
  # we can easily query the metadata aof a given journal.
  #
  # For instance, from this:
  #
  # [{
  #     "Journal name" => "Nice journal",
  #     "ISSN" => "1053-8119",
  #     "IF 2014" => "1.532"
  # },
  # {
  #     "Journal name" => "Better journal",
  #     "ISSN" => "8721-0982",
  #     "IF 2014" => "2.375"
  # },…]
  #
  # To this:
  #
  # {
  #     "Nice journal" => {
  #         :issn => "1053-8119"
  #         :if_2014 => "1.532"
  #     },
  #     "Better journal" => {
  #         :issn => "8721-0982"
  #         :if_2014 => "2.375"
  #     },
  #     …
  # }
  journal_hash = Hash[journal_list.map { |row|
    [
      row[CSV_HEADER_JOURNAL_NAME],
      {
        CSV_HEADER_ISSN => row[CSV_HEADER_ISSN],
        CSV_HEADER_IF => row[CSV_HEADER_IF]
      }
    ]
  }]

  entry_count = bibtex_content.length

  # Going through all the BibTeX entries.
  bibtex_content.each_with_index do |bibtex_entry, index|
    # If there is a journal entry and the isiwok field is "true",
    # we add/overwrite the field "note" with the appropriate data.
    if bibtex_entry.journal && bibtex_entry.isiwok == "OUI"
      # Getting the metadata associated with the journal name.
      journal_metadata = journal_hash[bibtex_entry.journal]

      # If the journal could not be found, we try once more, by removing
      # the first and the last characters, which may be braces.
      # bibtex-ruby only interprets the first level braces.
      if journal_metadata.nil?
        journal_metadata = journal_hash[bibtex_entry.journal.gsub(/^{|}$/, '')]

        # If the metadata were finally found, increase the conversion counter.
        unless journal_metadata.nil?
          number_of_double_braces_converted += 1
        end
      end

      # If the journal could not be found, we try once more, by converting
      # Latex escape sequence.
      if journal_metadata.nil?
        journal_metadata = journal_hash[bibtex_entry.journal.convert(:latex).to_s]

        # If the metadata were finally found, increase the conversion counter.
        unless journal_metadata.nil?
          number_of_double_braces_converted += 1
        end
      end

      if journal_metadata.nil?
        # If the journal wasn't found, add it to the set of missing journals.
        journals_not_found << bibtex_entry.journal.convert(:latex).to_s
      else
        bibtex_entry[:note] = "(#{NOTE_ISSN} : #{journal_metadata[CSV_HEADER_ISSN]}, #{NOTE_IF} : #{journal_metadata[CSV_HEADER_IF]})"
      end
    end

    # Now, we also need to underline the lastname of some people.
    bibtex_entry.author.each do |author|
      # Go through the entire list of name for each author line of the
      # BibTeX file.
      name_list.each do |row|
        last_name = row[CSV_HEADER_LAST_NAME]
        first_name = row[CSV_HEADER_FIRST_NAME]

        # Do we have corresponding firstname/lastname pairs?
        # Please not that this will need to be improved
        # in case people have suffix and prefix, for instance, someone
        # named : André van De Berck Jr.
        # "van" will be a prefix, while "Jr." will be a suffix.
        #
        # Also, "i" might be escaped in the bib, so we test for that case
        # as well.
        same_last_name =
          (last_name == author.last) || (last_name.gsub("\\i", "i") == author.last.gsub("\\i", "i"))

        same_first_name=
          (first_name == author.first) || (first_name.gsub("\\i", "i") == author.first.gsub("\\i", "i"))

        if (same_last_name && same_first_name)
          author.last = "\\underline{#{author.last}}"
          break
        end
      end
    end

    write_progress(index + 1, entry_count)
  end
  STDOUT.write "\n"

  print_and_flush "Writing the processed bibliography… "
  # Writing metadatas in the header of the file.
  out_file_path = path_to_updated_file_from(original_bibtex_file_path)
  out_file = File.open("#{path_to_updated_file_from(original_bibtex_file_path)}", 'w')
  out_file.write("% File generated with bibtex_upgrade.rb, on #{Time.now}.\n")
  out_file.write("% The file encoding is supposed to be ISO8859_1.\n\n")

  # Writing the back the processed bibliography
  out_file.write(bibtex_content.to_s.encode("ISO-8859-1", "UTF-8"))
  print_and_flush Paint["DONE\n", :green]

  if (number_of_double_braces_converted > 0)
    print_and_flush Paint["\nINFO: #{number_of_double_braces_converted} journal entries were processed through the LaTeX filter.\n", :blue]
  end

  # Writing the list of missing journals, if applicable.
  unless journals_not_found.empty?
    print_and_flush Paint["\nWARNING: Some journal entries could not be found in \"#{File.basename(journals_file_path)}\".\n", :yellow]

    journals_not_found.to_a.each do |journal|
      print_and_flush "\t#{journal}\n"
    end
  end

  puts "\n#{Paint["Finished!", :green]} - cmd+click to open the file: #{Paint[out_file_path, :blue, :underline]}"
end

##########################

if ARGV.size < 3
  puts "VERSION: #{SCRIPT_VERSION}"
  puts "USAGE: ./bibtex_upgrade <year> bibliography.bib name_list.csv journal_list.csv"
  exit
end

original_bibtex_file_path = ARGV[0]
names_file_path = ARGV[1]
journals_file_path = ARGV[2]

puts "#{Paint["INFO: Using Ruby version #{RUBY_VERSION}", :blue]}"

# Do the magic.
process_file(original_bibtex_file_path, names_file_path, journals_file_path)
