# CRAN's BibTeX normalizer

Normalize CRAN's bibliography.

It's written in Ruby and uses the gem [BibTeX-Ruby].

A gem is a reusable, packaged library, made available through [Rubygem, the Ruby Package Manager].

[Rubygem, the Ruby Package Manager]: https://rubygems.org/
[BibTeX-Ruby]: https://github.com/inukshuk/bibtex-ruby

## Installation

### Installing a recent version of Ruby on macOS.

macOS has historically shipped with outdated Ruby runtimes and since macOS 10.15 (Catalina),
all the scripting language runtimes are now [deprecated]. This utility supports ruby >=2.4.0.

Here are the steps to install a recent version of Ruby on macOS.

- Open your favorite Terminal emulator.

- Install Xcode command line tools (they are required for [Homebrew] and [Ruby])

```bash
xcode-select --install
```

- Open [https://brew.sh/](https://brew.sh/) in your favorite browser and
follow the instructions to install Homebrew. Homebrew is a package manager for macOS.

- Install the latest version of [Ruby]

```bash
brew install ruby
```

Since homebrew's ruby is keg only, the appropriate $PATH needs to be set to
make sure it takes precedence over system's ruby. The first thing you
should do is execute this command in your terminal:

```bash
export PATH="`brew --prefix`/opt/ruby/bin:$PATH"
```

This will set the path for the duration of the session (i. e. as long as
your terminal window is opened) and won't impact the rest of the system.

[deprecated]: https://developer.apple.com/documentation/macos_release_notes/macos_catalina_10_15_release_notes#3318257]
[Homebrew]: https://brew.sh/
[Ruby]: https://www.ruby-lang.org/

### Installing the required gems

⚠️ **Before installing gems, don't forget to run the command below!** ⚠️

```bash
export PATH="`brew --prefix`/opt/ruby/bin:$PATH"
```

**Short version:** just run the commands below.

```bash
gem install bundler
bundle install
```
**Long version:** please see below.

#### Installing gem manually

Gem can usually be installed through the following command:

```bash
gem install <gem-name>
```

However, managing appropriate versions of a gemset is an annoyance, especially
when it comes to out-of-date ruby runtimes, which require specific versions of
the aforementioned gems.

#### Enter Bundler

In order to ease the pain of managing the gem dependency, this utility uses [Bundler].
Bundler is actually a gem, and the last one you'll ever have to install manually.

```bash
gem install bundler
```

Bundler installs a set of gem dependencies specified in a file named `Gemfile`.
Usually, things go smoothly, and Bundler is run with the following command:

```bash
bundle install
```
Bundle downloads all gem specified in the file and resolves all dependencies.

## Executing the utility

⚠️ **Before installing gems, don't forget to run the command below!** ⚠️

```bash
export PATH="`brew --prefix`/opt/ruby/bin:$PATH"
```

Run the command below:

```bash
./bibtex_upgrade bibliography.bib name_list.csv journal_list.csv
```

## FAQ

### What does “INFO: X journal entries were processed through the LaTeX filter.” mean?
It means the utility encountered X journal tags surrounded by double braces and converted them through
the built-in LaTeX filter, in order to match the given names provided by the list of journals.

### What does “WARNING: Some journal entries could not be found in …” mean?
It means that the journal tag (found inside an entry containing `isiwok = {OUI}`)
is missing from the provided list of journals.
