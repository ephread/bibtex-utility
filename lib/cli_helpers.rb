# Path to the processed file,
# built from the original file.
def path_to_updated_file_from(path)
  directory = File.dirname(path)
  basename = File.basename(path)

  return "#{directory}/REPLACED_#{basename}"
end

# That's just here to print some stuff on the console,
# without newline at the end.
def print_and_flush(str)
  print str
  $stdout.flush
end

# Write current progress on terminal
def write_progress(current_line, line_count)
  $stdout.write "\rProcessing: #{(((1.0 * current_line) / line_count) * 100).round.to_i}% - #{current_line}/#{line_count}"
  $stdout.flush
end
